# random-collected-writing

## Tip: Python on Windows in Git Bash

This isn't necessarily hacky, just a little clumsy relative to how it feels like it should work.  That being said, using a venv or conda environment are both good practices, so this is just a little bit snake-eating-its-own-tail in that we're bootstrapping a virtualenv (less functionality than conda) via a full conda install.  Like trading a ferrari for a ford pinto.

- Install full anaconda (miniconda not as batteries included here and probably won't work with these instructions)
- Open a base environment using Anaconda navigator and execute the following
    - `pip install virtualenv`
    - `python -m virtualenv my-environment`
- Go back to your Git Bash window and navigate to `my-environment/Scripts`
- Run the following in Git Bash:
    - `source activate`

You should now have your new environment active have have `python` and `pip` available in git bash.
